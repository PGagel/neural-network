#include "Net.hpp"
#include <iostream>
#include <Net.hpp>

Net::Net(int depth, const int* perceptrons_per_level, float learning_rate) {
    this->depth = depth;
    this->level_perceptron_counts = new int[depth];
    this->learning_rate = learning_rate;

    // initialize perceptron array according to depth and perceptron count per level
    this->perceptrons = new Perceptron**[depth];
    for(int level = 0; level < depth; level++){
        int curr_perceptron_count = perceptrons_per_level[level];

        this->level_perceptron_counts[level] = curr_perceptron_count;
        this->perceptrons[level] = new Perceptron*[curr_perceptron_count];

        for(int perc = 0; perc < curr_perceptron_count; perc++){
            // When this is the first layer, all perceptrons have have one input
            if (level == 0)
                this->perceptrons[level][perc] = new Perceptron(1, learning_rate);
            // Else they have as many inputs as number of neurons in the previous layers
            else
                this->perceptrons[level][perc] = new Perceptron(perceptrons_per_level[level-1], learning_rate);
        }
    }
}

Net::Net(int depth, const int *perceptrons_per_level, Perceptron ***perceptrons) {
    this->depth = depth;
    this->level_perceptron_counts = new int[depth];
    this->learning_rate = perceptrons[0][0]->get_learning_rate();

    for(int level = 0; level < depth; level++)
        this->level_perceptron_counts[level] = perceptrons_per_level[level];

    this->perceptrons = perceptrons;
}

Net::~Net() {

    // Cleanup perceptrons array
    for (int level = 0; level < depth; level++){
        int curr_perceptron_count = this->level_perceptron_counts[level];
        for(int perc = 0; perc < curr_perceptron_count; perc++){
            delete this->perceptrons[level][perc];
        }
        delete[] this->perceptrons[level];
    }
    delete[] this->perceptrons;

    // Cleanup size array
    delete[] this->level_perceptron_counts;
}

float* Net::guess(const float* input) {

    // initialize results
    float* result = nullptr;
    auto step = const_cast<float *>(input);
    // forward feed information through net
    for(int level = 0; level < this->depth; level++) {

        // save number of perceptrons in this layer
        int perc_count = this->level_perceptron_counts[level];
        // initialize result array to pass on to the next layer
        result = new float[perc_count];

        // if this is the first level use each value of array as input for a perceptron
        if (level == 0)
            // feed information into every perceptron
            for (int perc = 0; perc < perc_count; perc++)
                result[perc] = this->perceptrons[level][perc]->guess(step++);
        else {
            // feed information into every perceptron
            for (int perc = 0; perc < perc_count; perc++) {
                result[perc] = this->perceptrons[level][perc]->guess(step);
            }
            delete[] step;
        }
        // input information for next layer = result of this layer
        step = result;
    }
    // return result of last layer as final result
    return result;
}

float Net::train(const float* input, float* expected_results) {
    //
    auto actual_results = new float*[this->depth];
    auto step = const_cast<float*>(input);

    // forward feed information through net and save all results.
    for(int level = 0; level < this->depth; level++) {

        // save number of perceptrons in this layer
        int perc_count = this->level_perceptron_counts[level];
        // initialize result array to pass on to the next layer
        actual_results[level] = new float[perc_count];

        // if this is the first level use each value of array as input for a perceptron
        if (level == 0)
            // feed information into every perceptron
            for (int perc = 0; perc < perc_count; perc++)
                actual_results[level][perc] = this->perceptrons[level][perc]->guess(step++);
        else
            // feed information into every perceptron
            for (int perc = 0; perc < perc_count; perc++)
                actual_results[level][perc] = this->perceptrons[level][perc]->guess(step);

        // save result of all layers
        step = actual_results[level];
    }

    // Calculate errors at output layer
    auto errors = new float[this->level_perceptron_counts[this->depth - 1]];
    // and sum up errors to get MSE
    float mse = 0;
    for(int perc = 0; perc < this->level_perceptron_counts[this->depth - 1]; perc++) {
        float error = actual_results[this->depth - 1][perc] - expected_results[perc];
        errors[perc] = error;
        mse += error * error;
    }
    // Divide summed up Squared error by number of output perceptrons to get Mean SE
    mse /= this->level_perceptron_counts[this->depth - 1];

    // Only start training if there was actually an error
    if (mse>0) {

        auto gamma_prev = new float[this->level_perceptron_counts[this->depth - 1]];

        // Calculate initial gamma
        for(int perc = 0; perc < this->level_perceptron_counts[this->depth - 1]; perc++){
            gamma_prev[perc] = errors[perc] * this->perceptrons[this->depth - 1][perc]->activation_deriv(actual_results[this->depth - 1][perc]);
        }
        delete[] errors;

        // Backpropagate through Net and adjust weights
        for (int level = this->depth - 1; level >= 0; level--) {
            // Get number of perceptrons in this and the previous layer
            int perc_count = this->level_perceptron_counts[level];
            int next_perc_count = level != 0 ? this->level_perceptron_counts[level - 1] : 1;

            // Calculate weight adjustments
            auto dw = new float[next_perc_count];
            for(int perc = 0; perc < perc_count; perc++){
                float dw_i = gamma_prev[perc] * actual_results[level][perc];

                for(int next_perc = 0; next_perc < next_perc_count; next_perc++){
                    dw[next_perc] = dw_i;
                }

                this->perceptrons[level][perc]->train(dw);
            }
            delete[] dw;

            // If there is still a previous layer calculate gamma for it
            if(level != 0) {

                // Calculate the gammas for the previous layer
                auto gamma = new float[next_perc_count];
                for (int next_perc = 0; next_perc < next_perc_count; next_perc++) {

                    // Calculate SUM[i in range(perc_count)] (gamma[i] * weight[])
                    float prev_adjust_sum = 0;
                    for(int perc = 0; perc < perc_count; perc++) {
                        prev_adjust_sum += gamma_prev[perc] * this->perceptrons[level][perc]->get_weights()[next_perc];
                    }

                    gamma[next_perc] =  this->learning_rate *
                                        this->perceptrons[level - 1][next_perc]->activation_deriv(actual_results[level - 1][next_perc]) *
                                        prev_adjust_sum;
                }

                delete[] gamma_prev;
                gamma_prev = gamma;
            }
        }
        // cleanup
        delete[] actual_results;
        delete[] gamma_prev;
    }
    return mse;
}
