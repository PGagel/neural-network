#include <iostream>
#include <vector>
#include <random>
#include <chrono>

#include "Point.hpp"
#include "CSVReader.hpp"

#include "Perceptron.hpp"
#include "Net.hpp"

using namespace std;
using namespace std::chrono;

// Helper method to print progress bar
void print_progress(int current, int max){

    int progress_bar_length = 50;
    int filled_length = static_cast<int>((float)(current) / max * (progress_bar_length+3));
    char str[progress_bar_length + 3];

    str[0] = '[';
    for(int i = 1; i < progress_bar_length + 1; i++){
        char c;
        if(filled_length >= i) c = '=';
        else c = ' ';
        str[i] = c;
    }
    str[progress_bar_length + 1] = ']';
    str[progress_bar_length + 2] = '\0';

    cout << str;
}

int main(int argc, char* argv[]){
	string path = "../data/winequality-white.csv";
    CSVReader data(';');
    data.read(path);

    if(data.is_well_bahaved()) {
        // get number of input parameters
        int input_count = static_cast<int>(data.get_bounds().cols[0]) - 1;
        // get number of data sets in file
        int data_sets_count = static_cast<int>(data.get_bounds().rows);
        // specify number of data sets used to train the net
        int training_sets_count = data_sets_count - 101;

        // Specify number of layers (including input and output layer)
        int depth = 4;
        // Specify number of neurons per layer
        const int sizes[] = {input_count, input_count*2, input_count, 1};
        // Specify learning rate
        float learning_rate = 0.01;

        // Create neural network with upper specifications
        Net net(depth, sizes, learning_rate);

        // variable to calculate average mse
        double average_mse = 0;
        // Training (from index 1 (first data line) to number of training sets)
        for(int training_id = 1; training_id < training_sets_count; training_id++){

            // for all columns expect the last in this row, parse the value to float and save it
            auto input = new float[input_count];
            for(int col = 0; col < input_count; col++) {
                input[col] = data.get<float>(training_id, col);
            }

            // Save the expected result
            auto result = data.get<float>(training_id, input_count) / 10.0f;

            // Train the net with the inputs and predefined result.
            float mse = net.train(input, &result);
            // sum up mse
            average_mse += mse;

            delete[] input;

            // print progress
            print_progress(training_id, training_sets_count);
            cout << mse * 10;
            cout << '\r';
        }
        // print average MSE
        cout << endl << "Average MSE: " << average_mse/(training_sets_count-1) * 10 << endl;

        // Guessing (from first untrained data set to last)
        for(int guessing_id = training_sets_count; guessing_id < data_sets_count; guessing_id++) {

            // for all columns expect the last in this row, parse the value to float and save it
            auto input = new float[input_count];
            for(int col = 0; col < input_count; col++)
                input[col] = data.get<float>(guessing_id, col);

            // Save result for comparison
            auto result = data.get<float>(guessing_id, input_count);

            // Let net guess with inputs
            float* guess = net.guess(input);
            delete[] input;

            // Print the actual result next to the expected result.
            cout << "Guess: " << guess[0] * 10 << " : Expected: " << result << endl;
        }
        cout << endl << "FINISHED!" << endl;
    }
	return 0;
}
