
#include <Perceptron.hpp>

#include "Perceptron.hpp"

Perceptron::Perceptron(int input_count, float* weights, float learning_rate){
	this->input_count = input_count;
	this->weights = weights;
	this->learning_rate = learning_rate;
}

Perceptron::Perceptron(int input_count, float learning_rate) {
	this->input_count = input_count;
	this->weights = new float[input_count];
	this->learning_rate = learning_rate;

	// assign weights randomly
	std::default_random_engine gen(static_cast<unsigned long>(std::chrono::system_clock::now().time_since_epoch().count()));
	std::uniform_real_distribution<float> dist(-1.0f,1.0f);

	for(int i = 0; i < input_count; i++){
		weights[i] = dist(gen);
	}
}

float Perceptron::guess(float* input){
	// sum variable for result
    float result = 0;

    // sum up result
	for(int i = 0; i < this->input_count; i++){
		result += input[i] * weights[i];
	}
	// return result of activation function
	return activation(result);
}

void Perceptron::train(float* dw){
    // adjust each weight
	for(int i = 0; i < this->input_count; i++){
	    this->weights[i] -= dw[i];
	}
}

Perceptron::~Perceptron() {
    delete[] this->weights;
}

