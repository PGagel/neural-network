//
// Created by philipp on 21.12.18.
//
#include "CSVReader.hpp"

#include <vector>
#include <algorithm>
#include <string.h>
#include <CSVReader.hpp>


CSVReader::CSVReader(char separator)  : separator(separator), well_behaved(true) {}

Bounds CSVReader::check_boundaries(string& path){
    // Open file
    ifstream reader(path);
    string line;

    // initialize counter
    ulong row_counter = 0;
    vector<ulong> col_counts;
    Bounds b{0, nullptr};

    // while there is still a line
    while(getline(reader, line)){
        // increase row counter
        row_counter ++;
        // count number of columns in this row
        col_counts.push_back((ulong)(count(line.begin(), line.end(), separator)) + 1);
    }

    // if the file is empty return empty bounds.
    if(row_counter == 0) return b;

    // initialize bounds struct
    b.rows = row_counter;
    b.cols = new ulong[b.rows];
    // save number of columns in head
    ulong cols_head = col_counts.at(0);
    for(ulong row = 0; row < b.rows; row++){

        ulong cols_curr = col_counts.at(row);
        // if the current number of columns differs from the head count the data is not well behaved
        if(cols_curr != cols_head) this->well_behaved = false;
        // set column count for this row
        b.cols[row] = col_counts.at(row);
    }

    return b;
}

void CSVReader::read(string path) {

    // Before reading data check bounds
    this->bounds = check_boundaries(path);

    // if the file is empty, don't start at all
    if(this->bounds.rows == 0) return;

    // initialize file reader
    ifstream reader(path);

    // initialize data array with number of rows
    this->data = new char**[this->bounds.rows];
    // read and parse each row
    for(int row = 0; row < this->bounds.rows; row++){

        // save current number of columns
        ulong cols = this->bounds.cols[row];
        // initialize data array accordingly
        this->data[row] = new char*[cols];

        string line;
        // get next line
        getline(reader, line);
        // save end of this line
        const char* end = &line.c_str()[line.size()];
        // initialize iteration variable
        char* last_sep = const_cast<char*>(line.c_str());

        // split row into columns
        for(int col = 0; col < cols; col++){

            // look for the next occurrence of the separator
            char* this_sep = strchr(last_sep, this->separator);
            ulong size;

            // if there is no next separator, this is the last column
            if(this_sep == nullptr) size = end - last_sep;
            // else get length between the last and this occurrence
            else size = this_sep - last_sep;

            // initialize this specific cell with the calculated size
            this->data[row][col] = new char[size + 1];

            // copy the data from line into cell
            memcpy(this->data[row][col], last_sep, size);

            // null terminate cell
            this->data[row][col][size] = '\0';

            // index of last separator is the next index of this separator because it has to be ignored.
            last_sep = this_sep + 1;
        }
    }
}

// All get methods are basically the same. They only differ in the selected cast method.
// They get the string of the specified cell and cast it.
// The only exception is the char* implementation since there is no necessity to cast the string. Instead the char array gets copied;
template <>
char* CSVReader::get(int row, int col) {
    char* start = this->data[row][col];
    char* end = strchr(start, '\0');

    auto size = static_cast<size_t>(end - start + 1);

    char* cell = new char[size];
    mempcpy(cell, start, size);

    return cell;
}

template <>
double CSVReader::get(int row, int col){
    char* cell = this->data[row][col];
    return strtod(cell, nullptr);
}

template <>
float CSVReader::get(int row, int col){
    char* cell = this->data[row][col];
    return strtof(cell, nullptr);
}

template <>
int CSVReader::get(int row, int col){
    char* cell = this->data[row][col];
    return stoi(cell);
}

template <>
long CSVReader::get(int row, int col){
    char* cell = data[row][col];
    return strtoq(cell, nullptr, 10);
}

