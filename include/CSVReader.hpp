#pragma once
#include <string>
#include <fstream>
using namespace std;

struct Bounds{
    ulong rows;
    ulong* cols;
};

class CSVReader {

private:
    // Data to read from
    char*** data;
    // Bounds of data
    Bounds bounds;
    // Sizes of cells
    int** sizes;
    // Path to file to read
    char separator;
    // Determines if all rows have the same length as the header row
    bool well_behaved;

    Bounds check_boundaries(string& path);

public:
    explicit CSVReader(char separator = ';');

    // Get the raw string data in the format: row x col
    const char*** get_data() { return (const char***)(this->data); }
    // Get number of rows and number of cells per row
    Bounds get_bounds() { return this->bounds; }
    // Get current separator for data cells
    char get_separator() { return this->separator; }
    // Returns if all rows have the same number of cells
    bool is_well_bahaved() { return this->well_behaved; }

    // Read the file specified by path
    void read(string path);

    // Return value of specified cell as specified type
    template <typename T>
    T get(int row, int col);

};
