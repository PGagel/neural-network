#pragma once

class Point{
	private:
		int x, y;
		int expected_result;
	public:
		Point(int x, int y) : x(x), y(y), expected_result(x > y ? 1 : -1){};

		int get_x() { return this->x; }
		int get_y() { return this->y; }
		int get_expected_result() { return this->expected_result; }
};
