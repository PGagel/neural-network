#pragma once

#include <sstream>

#include "Perceptron.hpp"

class Net {
private:
    // Number of layers including in- and output layer
    int depth;
    // Number of perceptrons per layer
    int* level_perceptron_counts;
    // Learning rate
    float learning_rate;
    // Array of perceptrons
    Perceptron*** perceptrons;

public:
    // ======== Constructors ======== //
    Net (int depth, const int* perceptrons_per_level, float learning_rate);
    Net (int depth, const int* perceptrons_per_level, Perceptron*** perceptrons);
    ~Net();

    // ======== Setter & Getter ======== //
    // Set learning rate
    void set_learning_rate(float learning_rate){ this->learning_rate = learning_rate; }

    // ======== Methods ======== //
    // get results of inputs
    float* guess(const float* input);
    // train net with inputs anc expected output
    float train(const float* input, float* expected_results);

    // ======== Operator Overwrites ======= //
    // Overwrite ostream operator to cout net
    friend std::ostream& operator<<(std::ostream& os, const Net& net){
        os << "\"Net\":\n{\n\t";
        for(int i = 0; i < net.depth; i++){
            os << "\"Level_" << i << "\":\n\t{\n";
            int curr_perc_count = net.level_perceptron_counts[i];
            for(int j = 0; j < curr_perc_count; j++){
                os << net.perceptrons[i][j] << "\n";
            }
            os << "\t}\n";
        }
        os << "}\n";
        return os;
    }
};
