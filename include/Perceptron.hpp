#pragma once

#include <sstream>
#include <random>
#include <chrono>
#include <math.h>

class Perceptron {
private:
    // Static parameters for neural learning
    static constexpr float s = 0.2;
    static constexpr float t = 0.0;

    // number of input values
	int input_count;
	// weights of the input
	float *weights;
	// learning rate
	float learning_rate;


public:
	// ======== Constructors ======== //
	Perceptron(int input_count, float *weights, float learning_rate);
	Perceptron(int input_count, float learning_rate);
	Perceptron() : input_count(0), weights(new float[0]), learning_rate(0.0f) {}
	~Perceptron();

	// ======== Methods ======== //

	// activation function maps the result between to a floating point 0 and 1
    float activation(float result){
        return 1 / (1 + exp(-2 * Perceptron::s * (result + Perceptron::t)));
    }

    // derived activation function to train the net
    float activation_deriv(float x) {
        float s2 = s * 2;
        float e2sx = exp(s2*x);
        return static_cast<float>((s2 * e2sx) / pow(e2sx + 1, 2));
    }

    // Get result of inputs
	float guess(float *input);

    // Adjust weights according to array
    void train(float* dw);

	// ======== Getter & Setter ======== //
	// returns number of input
	int get_input_count() { return this->input_count; }

	// return weights
	float *get_weights() { return this->weights; }

	// return learning rate
	float get_learning_rate() { return this->learning_rate; }

	// ======== Operator Overwrites ======= //
	// Overwrite ostream operator to cout neuron
	friend std::ostream& operator<<(std::ostream& os, const Perceptron& per){
		os << "\"Perceptron\":\n{\n\t\"input_count\": " << per.input_count << "\n\t\"weights\": [";
		for(int i = 0; i < per.input_count; i++){
			os << per.weights[i];
			if(i != per.input_count -1) os << ", ";
		}
		os << "]\n\t\"learning_rate\": " << per.learning_rate << "\n}\n";
		return os;
	}
};


